package org.example.Lesson23.config;

import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * class MySpringMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
 * заменяет файл src/main/webapp/WEB-INF/w1eb.xml
 */
public class MySpringMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        /**
         * Т.к. класс использовать не будем, то возвращаем null
         */
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        /**
         * return new Class[]{SpringConfig.class}; - Возвращает, тоже что файл w1eb.xml :
         *
         *     <init-param>
         *       <param-name>contextConfigLocation</param-name>
         *
         *       <!-- В файле applicationContextMVC.xml будем при помощи XML конфигурировать наше SpringMVC-приложение   -->
         *       <param-value>/WEB-INF/applicationContextMVC.xml</param-value>
         *     </init-param>
         *
         *     Теперь class MySpringMvcDispatcherServletInitializer(испоняет роль w1eb.xml) знает, где
         *     находится Spring-конфигурация (SpringConfig - испоняет роль applicationContextMVC.xml)
         */
        return new Class[]{SpringConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        /**
         * return new String[]{"/"}; - Возвращает, тоже что файл w1eb.xml :
         *
         * <!-- При помощи тега servlet-mapping обращаемся к DispatcherServlet   -->
         *   <servlet-mapping>
         *     <servlet-name>dispatcher</servlet-name>
         *
         *     <!-- / - Слэш, означает что любой URL который набирает пользователь в браузере, когда обращается к нашему серверу-->
         *     <!-- любой этот запрос на любой URL должен перенаправляться на DispatcherServlet  -->
         *     <!-- это будет означать, что запрос попал в SpringMVC-приложение -->
         *     <url-pattern>/</url-pattern>
         *   </servlet-mapping>
         */
        return new String[]{"/"};
    }

    /**
     * Методы:
     * onStartup()
     * registerHiddenFieldFilter()
     * реализуют возможность, чтобы заработали PATCH, DELETE запросы
     * а также фильтр сможет читать скрытое поле _method
     * @param aServletContext
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext aServletContext) throws ServletException {
        super.onStartup(aServletContext);
        registerHiddenFieldFilter(aServletContext);
    }

    private void registerHiddenFieldFilter(ServletContext aContext) {
        aContext.addFilter("hiddenHttpMethodFilter",
                /**
                 * символы /* -означают что фильтр работает для всех HTTP-запросов
                 */
                new HiddenHttpMethodFilter()).addMappingForUrlPatterns(null ,true, "/*");
    }
}
